#Autor: Aline Capucho
#Data: 01/10/2019
import math

def eq():
    print("Formato da equacao: a(x^2) + b(x) + c = 0\n")
    a = int(input("Insira o coeficiente a: "))
    b = int(input("Insira o coeficiente b: "))
    c = int(input("Insira o termo independente c: "))
    delta = (b*b) - 4 * a * c
    if(delta<0):
        print("\nNão há raízes reais.")
        return 1
    elif(delta>0):
        x1 = (-b + math.sqrt(delta))/(2 * a)
        x2 = (-b - math.sqrt(delta))/(2 * a)
        print("\nHá duas raízes reais, sendo elas", x1, "e", x2)
        return 0
    else:
        x1 = -b/(2 * a)
        print("\nHá uma raíz, sendo ela real e igual a", x1)
        return 0

def main():
    eq()

main()