#Autor: Aline Capucho
#Data: 01/10/2019
soma = 0 #calcula o valor da sequencia
it = 0 #iteracao
opr = 1 #corrige a operacao a ser feita, + ou -
pi1 = 4#sera o valor de pi na iteracao atual
pi2 = 4#sera o valor de pi na iteracao anterior
while(((pi1 - pi2 >(5*(10**-8)) and (pi1>pi2))) or ((pi2-pi1)>(5*(10**-8)) and (pi2>pi1)) or pi1==pi2):
    pi2 = pi1
    soma+=opr*(1/(1+(2*it)))
    pi1 = 4*soma
    opr*=-1
    it+=1
print("O valor de pi e", pi1)